package com.epam.courses.model.task2;

@FunctionalInterface
public interface ElectricalNetworkOperation {

  String execute();
}

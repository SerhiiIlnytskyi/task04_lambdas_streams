package com.epam.courses.model.task2;

import com.epam.courses.model.task1.FirstTask;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ElectricalNetworkInvoker {
  private static Logger log = LogManager.getLogger(ElectricalNetworkInvoker.class);
  public final List<ElectricalNetworkOperation> electricalNetworkOperations = new ArrayList<>();

  public String executeOperation(ElectricalNetworkOperation electricalNetworkOperation) {
    electricalNetworkOperations.add(electricalNetworkOperation);
    log.info("Execute operation: " + electricalNetworkOperation.toString());
    return electricalNetworkOperation.execute();
  }
}

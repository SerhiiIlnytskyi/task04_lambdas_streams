package com.epam.courses.model.task2.model;

public abstract class ElectricalDevice {

  public abstract String turnOff();

  public abstract String turnOn();
}

package com.epam.courses.model.task2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TableLamp extends ElectricalDevice {
  private static Logger log = LogManager.getLogger(TableLamp.class);


  @Override
  public String turnOff() {
    log.info("Task2. Called method turnOff");
    return "Turn off the light";
  }

  @Override
  public String turnOn() {
    log.info("Task2. Called method turnOn");
    return "Turn on the light";
  }
}

package com.epam.courses.model.task2;

import com.epam.courses.model.task2.model.ElectricalDevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TurningOnElectricity implements ElectricalNetworkOperation {
  private static Logger log = LogManager.getLogger(TurningOnElectricity.class);

  private ElectricalDevice electricalDevice;

  public TurningOnElectricity(ElectricalDevice electricalDevice) {
    this.electricalDevice = electricalDevice;
  }

  @Override
  public String execute() {
    log.info("Executing turning off electricity");
    return electricalDevice.turnOn();
  }

  @Override
  public String toString() {
    return "TurningOnElectricity{" +
        "electricalDevice=" + electricalDevice +
        '}';
  }
}

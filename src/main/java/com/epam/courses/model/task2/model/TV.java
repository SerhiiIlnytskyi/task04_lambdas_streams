package com.epam.courses.model.task2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TV extends ElectricalDevice {
  private static Logger log = LogManager.getLogger(TV.class);


  @Override
  public String turnOff() {
    log.info("Task2. Called method turnOff");
    return "TV turned off";
  }

  @Override
  public String turnOn() {
    log.info("Task2. Called method turnOn");
    return "TV turned on";
  }
}

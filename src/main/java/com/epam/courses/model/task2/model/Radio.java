package com.epam.courses.model.task2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Radio extends ElectricalDevice {

  private static Logger log = LogManager.getLogger(Radio.class);


  @Override
  public String turnOff() {
    log.info("Task2. Called method turnOff");
    return "Turn off the radio";
  }

  @Override
  public String turnOn() {
    log.info("Task2. Called method turnOn");
    return "Turn on the radio";
  }
}

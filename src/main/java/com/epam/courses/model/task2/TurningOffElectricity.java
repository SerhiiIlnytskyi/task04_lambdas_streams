package com.epam.courses.model.task2;

import com.epam.courses.model.task2.model.ElectricalDevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TurningOffElectricity implements ElectricalNetworkOperation {
  private static Logger log = LogManager.getLogger(TurningOffElectricity.class);

  private ElectricalDevice electricalDevice;

  public TurningOffElectricity(ElectricalDevice electricalDevice) {
    this.electricalDevice = electricalDevice;
  }

  @Override
  public String execute() {
    log.info("Executing turning off electricity");
    return electricalDevice.turnOff();
  }

  @Override
  public String toString() {
    return "TurningOffElectricity{" +
        "electricalDevice=" + electricalDevice +
        '}';
  }
}

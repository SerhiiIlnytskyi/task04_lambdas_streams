package com.epam.courses.model.task3;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThirdTask {

  private static Logger log = LogManager.getLogger(ThirdTask.class);

  private List<Integer> valuesList;

  public ThirdTask(List<Integer> valuesList) {
    this.valuesList = valuesList;
  }

  public double average() {
    log.info("Calculate average");
    return valuesList.stream()
        .mapToInt(Integer::intValue)
        .average()
        .getAsDouble();
  }

  public int min() {
    log.info("Calculate min");
    return valuesList.stream()
        .mapToInt(Integer::intValue)
        .min()
        .getAsInt();
  }

  public int max() {
    log.info("Calculate max");
    return valuesList.stream()
        .mapToInt(Integer::intValue)
        .max()
        .getAsInt();
  }

  public double sum() {
    log.info("Calculate sum through sum()");
    return valuesList.stream()
        .mapToInt(Integer::intValue)
        .sum();
  }

  public int sumThroughReduce() {
    log.info("Calculate sum through reduce");
    return valuesList.stream().reduce(0, (a, b) -> a + b);
  }

  public long countBiggerThenAverage() {
    log.info("Calculate bigger then average");
    double average = this.average();
    return valuesList.stream()
        .filter(value -> value > average)
        .count();
  }
}

package com.epam.courses.model.task3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ListGenerators {

  private static Logger log = LogManager.getLogger(ListGenerators.class);

  public List<Integer> streamOfGenerator() {
    Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
    log.info("Generated stream. Method - Stream.of");
    return stream.collect(Collectors.toList());
  }

  public List<Integer> streamIteratorGenerator() {
    Stream<Integer> stream = Stream.iterate(20, n -> n * 2).limit(10);
    log.info("Generated stream. Method - Stream.iterate");
    return stream.collect(Collectors.toList());
  }

  public List<Integer> streamArraysGenerator() {
    Stream<Integer> stream = Arrays.asList(1, 2, 3, 4, 5, 6, 7).stream();
    log.info("Generated stream. Method - Arrays.asList(...).stream()");
    return stream.collect(Collectors.toList());
  }

  public List<Integer> streamBuildereGenerator() {
    Stream<Integer> stream = Stream
        .<Integer>builder()
        .add(1)
        .add(2)
        .add(3)
        .add(4)
        .add(5)
        .build();
    log.info("Generated stream. Method - Stream.builder()");
    return stream.collect(Collectors.toList());
  }
}

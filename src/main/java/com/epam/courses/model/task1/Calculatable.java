package com.epam.courses.model.task1;

@FunctionalInterface
public interface Calculatable {

  int calculate(int firstValue, int secondValue, int thirdValue);
}

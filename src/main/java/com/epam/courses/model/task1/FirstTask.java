package com.epam.courses.model.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstTask {
  private static Logger log = LogManager.getLogger(FirstTask.class);

  Calculatable maxValue = (a, b, c) -> (Math.max(a, Math.max(b, c)));
  Calculatable average = (a, b, c) -> (a + b + c) / 3;

  public int maxValue(int a,int b,int c) {
    log.info( String.format("Task1. Called method maxValue with arguments %d %d %d", a, b, c));
    return maxValue.calculate(a, b, c);
  }

  public double average(int a, int b, int c) {
    log.info( String.format("Task1. Called method average with arguments %d %d %d", a, b, c));
    return average.calculate(a, b, c);
  }
}

package com.epam.courses.model.task4;

import static java.util.stream.Collectors.toMap;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FourthTask {

  private static Scanner INPUT = new Scanner(System.in);

  private String userInput = new String();

  public void readFromConsole() {
    StringBuilder readerResult = new StringBuilder();
    System.out.println("Please, write your text lines: ");
    String readLine;
    do {
      readLine = INPUT.nextLine();
      readerResult.append(readLine).append("\n");
    } while (!readLine.equals(""));
    this.userInput = readerResult.toString();
  }

  public long uniqueCount() {
    return Arrays.stream(userInput.split("[ \n]"))
        .map(String::trim)
        .distinct()
        .count();
  }

  public List<String> uniquingAndSortStrings() {
    return Arrays.stream(userInput.split("[ \n]"))
        .map(String::trim)
        .distinct()
        .sorted()
        .collect(Collectors.toList());
  }

  public Map<String, Integer> findWordOccurrencesCount() {
    return Arrays.stream(userInput.split("[ \n]"))
        .map(word -> new SimpleEntry<>(word, 1))
        .collect(toMap(e -> e.getKey(), e -> e.getValue(), (v1, v2) -> v1 + v2));
  }

  public Map<String, Long> findCharacterOccurrencesCountExceptUpperCase() {
    return Arrays.stream(userInput.split(""))
        .filter(character -> character.matches("[a-z]"))
        .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
  }
}

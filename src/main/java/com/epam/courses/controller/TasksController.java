package com.epam.courses.controller;

import com.epam.courses.model.task1.FirstTask;
import com.epam.courses.model.task2.ElectricalNetworkInvoker;
import com.epam.courses.model.task2.TurningOnElectricity;
import com.epam.courses.model.task2.model.ElectricalDevice;
import com.epam.courses.model.task2.model.Radio;
import com.epam.courses.model.task2.model.TV;
import com.epam.courses.model.task2.model.TableLamp;
import com.epam.courses.model.task3.ListGenerators;
import com.epam.courses.model.task3.ThirdTask;
import com.epam.courses.model.task4.FourthTask;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TasksController {
  private static Logger log = LogManager.getLogger(TasksController.class);

  private FirstTask firstTask;
  private ElectricalNetworkInvoker electricalNetworkInvoker;
  private ListGenerators listGenerators;
  private FourthTask fourthTask;

  public TasksController() {
    this.firstTask = new FirstTask();
    this.electricalNetworkInvoker = new ElectricalNetworkInvoker();
    this.listGenerators = new ListGenerators();
    this.fourthTask = new FourthTask();
  }

  public void testFirstTask() {
    log.info("First task test started");
    firstTask.average(1, 2, 3);
    firstTask.maxValue(1, 2, 3);
    log.info("First task test finished OK!!!");
  }

  public void testSecondTask() {
    log.info("Second task test started");

    log.info("Execute operation as object of command class");
    electricalNetworkInvoker.executeOperation(new TurningOnElectricity(new TableLamp()));
    electricalNetworkInvoker.executeOperation(new TurningOnElectricity(new Radio()));

    log.info("Execute operation as lambda function");
    electricalNetworkInvoker.executeOperation(() -> "Fire alarm is turned on");

    log.info("Execute operation as method reference");
    ElectricalDevice tv = new TV();
    electricalNetworkInvoker.executeOperation(tv::turnOn);

    log.info("Execute operation as anonymous class");
    ElectricalDevice radio = new Radio() {
      @Override
      public String turnOff() {
        return "Turn off the radio2";
      }

      @Override
      public String turnOn() {
        return "Turn on the radio2";
      }
    };
    electricalNetworkInvoker.executeOperation(radio::turnOn);
    electricalNetworkInvoker.executeOperation(radio::turnOff);
    log.info("Second task test finished OK!!!");
  }

  public void testThirdTask() {
    log.info("Third task test started");
    ThirdTask thirdTask1 = new ThirdTask(listGenerators.streamArraysGenerator());
    ThirdTask thirdTask2 = new ThirdTask(listGenerators.streamBuildereGenerator());
    ThirdTask thirdTask3 = new ThirdTask(listGenerators.streamIteratorGenerator());
    ThirdTask thirdTask4 = new ThirdTask(listGenerators.streamOfGenerator());

    thirdTask1.average();
    thirdTask2.countBiggerThenAverage();
    thirdTask3.max();
    thirdTask4.min();
    thirdTask1.sum();
    thirdTask1.sumThroughReduce();
    log.info("Third task test finished OK!!!");
  }

  public void testFourthTask() {
    log.info("Fourth task test started");
    fourthTask.readFromConsole();
    log.info("Unique words count " + fourthTask.uniqueCount());
    log.info("Unique and sorted words list" + fourthTask.uniquingAndSortStrings());

    Map<String, Integer> occurences = fourthTask.findWordOccurrencesCount();
    occurences.forEach((k, v) -> log.info(String.format("Word %s occurrences %d", k, v)));

    Map<String, Long> charsOccurences = fourthTask.findCharacterOccurrencesCountExceptUpperCase();
    charsOccurences.forEach((k, v) -> log.info(String.format("Char %s occurrences %d", k, v)));
    log.info("Fourth task test finished OK!!!");
  }
}

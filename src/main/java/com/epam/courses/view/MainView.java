package com.epam.courses.view;

import com.epam.courses.controller.TasksController;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {
  private static Scanner INPUT = new Scanner(System.in);
  private TasksController tasksController;
  private Map<String, String> menu;
  private Map<String, Executable> methodsMenu;

  public MainView() {
    tasksController = new TasksController();
    menu = new LinkedHashMap<>();
    menu.put("1", " 1 - Test first task");
    menu.put("2", " 2 - Test second task");
    menu.put("3", " 3 - Test third task");
    menu.put("4", " 4 - Test fourth task");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::firstTaskTest);
    methodsMenu.put("2", this::secondTaskTest);
    methodsMenu.put("3", this::thirdTaskTest);
    methodsMenu.put("4", this::fourthTaskTest);
  }

  private void firstTaskTest() {
    tasksController.testFirstTask();
  }

  private void secondTaskTest() {
    tasksController.testSecondTask();
  }

  private void thirdTaskTest() {
    tasksController.testThirdTask();
  }

  private void fourthTaskTest() {
    tasksController.testFourthTask();
  }


  private void pressButton0() {
    System.out.println("Good luck");
    INPUT.close();
    System.exit(0);
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    menu.values().forEach(System.out::println);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println(" Q - quit");
      System.out.print("Please, select menu point: ");
      keyMenu = INPUT.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).execute();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    this.pressButton0();
  }
}
